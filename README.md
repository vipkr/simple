# simple

A minimal, monospaced blogging theme for Hugo that respects privacy and is easy
on bandwidth.

*simple* is a fork of [smol](https://github.com/colorchestra/smol) created by
[colorchestra](https://github.com/colorchestra) and some feature adopted from
[sumnerevans's](https://github.com/sumnerevans/)
[smol](https://github.com/sumnerevans/smol) theme.


## Features

- No JavaScript.
- No spyware or tracking of any kind.
- No other external dependencies, embedded fonts or comment sections.
- Dark mode support (depending on your OS's setting).


## Installation

In your Hugo site `themes` directory, run:

```
git clone https://gitlab.com/vipkr/simple
```

Next, open `config.toml`, present in the root directory of the Hugo site and set
`theme` to `simple`.

```
theme = "simple"
```

Lastly, add the following lines to your `config.toml` to set site parameters and
make use of all the menu entries in the header and footer sections if you need
them.

```toml
# Parameters
[params]
    author = "Your Full Name"
    subtitle = "Your blog subtitle goes here!"
    dateFmt = "02.01.2006 15:04"

# Header
[menu]
  [[menu.main]]
        identifier = "posts"
        name = "Posts"
        url = "/posts/"
        weight = 1

  [[menu.main]]
        identifier = "categories"
        name = "Categories"
        url = "/categories/"
        weight = 2

  [[menu.main]]
        identifier = "tags"
        name = "Tags"
        url = "/tags/"
        weight = 3

  [[menu.main]]
        identifier = "rss"
        name = "RSS"
        url = "/index.xml"
        weight = 4

# Footer
  [[menu.footer]]
        name = "Github"
        url = "https://github.com/example"
        weight = 1

    [[menu.footer]]
        name = "Mastodon"
        url = "https://example.com/@user"
        weight = 2

    [[menu.footer]]
        name = "Imprint"
        url = "/imprint"
        weight = 3

```

For more information read the official [quick start
guide](https://gohugo.io/getting-started/quick-start/) of Hugo.


## Optional features
### Custom copyright text
Unlike `smol` theme, currently we cannot configure copyright text directly from
`config.toml`, as copyright notice is hard coded in
[footer.html](./layouts/partials/footer.html) file. If you want to change
notice, make changes in `footer.html` file, directly.

### Image captions
You can add captions to images (technically using `<figcaption>` HTML tags) by
adding titles, like so: `![Alt text here](/path/to/image.png "Put your caption
here!")`.


## Contributing

Have you found a bug or got an idea for a new feature? Feel free to use the
[issue tracker](https://gitlab.com/vipkr/simple/issues) to let me know. Or make
directly a [pull request](https://gitlab.com/vipkr/simple/merge_requests).


## License

This theme is released under the [MIT license](./LICENSE.md).
